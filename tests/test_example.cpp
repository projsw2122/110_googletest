
#include "../header/example.h"
#include "gtest/gtest.h" // also works well under windows

TEST(testMath, myCubeTest)
{
    EXPECT_EQ(1000, cubic(10));	
}

TEST(testMath, myBuggedCubeTest)
{
    EXPECT_EQ(1000, cubic_bugged(10));
}


 
int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}