#include <iostream>
#include "../header/example.h"

int main()
{
	double x;

	std::cout << "Enter a value:"; 
	std::cin >> x; // careful, no error handling
	std::cout << cubic(x) << std::endl;
	std::cout << cubic_bugged(x);
	return 0;
}