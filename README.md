# 110_googletest_small

Google Test Framework C++ example; tested in Windows 10 (PS and MSYS2) and Linux Xubuntu 20.04

# Very Small Example of C++ Unit Testing with GoogleTest 

"Software Design" course of M.EEC, in FEUP, Portugal

Main contributors:
 * Armando Sousa - (mailto:asousa@fe.up.pt)
 * Ricardo Marau - (mailto:marau@fe.up.pt)



## License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
In short, free software: Useful, transparent, no warranty


## Very small program with one function and its test

## Instalation and testing

Tested on Windows (power Shell) and Linux

Naturally, needs GoogleTest libraries

On windows https://packages.msys2.org/base :
````
pacman -S mingw-w64-x86_64-gtest
````

Also needs CMake
https://packages.msys2.org/package/mingw-w64-x86_64-cmake
````
pacman -S mingw-w64-x86_64-cmake
````

To setup, delete CMakeCache.txt, then:

````
cmake CMakeLists.txt
make
````



To run all the tests: `./runTests --gtest_color=yes`

To produce the executable (not the tests): `g++ -Wall src/*.cpp -o example.exe`




### References (additional information)

 * Google Test Wiki - http://code.google.com/p/googletest/wiki/Documentation

 * IBM A quick introduction to the Google C++ Testing Framework - http://www.ibm.com/developerworks/aix/library/au-googletestingframework.html

 * Google Test Installation Guide for C++ for Visual Studio Code - https://medium.com/swlh/google-test-installation-guide-for-c-in-windows-for-visual-studio-code-2b2e66352456

 * Google Test Release Git - https://github.com/google/googletest/tree/release-1.10.0

 * MSYS (compiler and package manager for windows, in order to achieve cross platform) 
    * https://www.msys2.org/wiki/MSYS2-installation/
    * https://packages.msys2.org/package/
    * https://packages.msys2.org/package/mingw-w64-x86_64-gtest
    * https://packages.msys2.org/package/mingw-w64-x86_64-gtest?repo=mingw64

To install Google Test on MSYS2, Windows: ` pacman -S mingw-w64-x86_64-gtest `

Try instructions above. If not working, then try instalations that you might need
  * https://packages.msys2.org/package/mingw-w64-x86_64-cmake?repo=mingw64
    * `pacman -S mingw-w64-x86_64-cmake`
  * https://packages.msys2.org/package/mingw-w64-x86_64-extra-cmake-modules?repo=mingw64
    * `pacman -S mingw-w64-x86_64-extra-cmake-modules`
  * https://packages.msys2.org/package/mingw-w64-x86_64-ninja?repo=mingw64
    * `pacman -S mingw-w64-x86_64-ninja`

  * Note that if you are using CMake and ninja, then instead of `make` you should `ninja`
    * In this mode, CMake produces a `build.ninja` file for the `ninja` tool


Manual compilation for the tests: `g++ -Wall src/code_?.cpp tests/*.cpp -lgtest -lgtest_main -lpthread -o runTests.exe`

Manual compilation with src/code_main.cpp and without the tests/test_example.cpp: `g++ -Wall src\*.cpp -o example.exe`
